package com.example.tp1;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class CountryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);

        Intent intent = getIntent();
        final String countryName = intent.getStringExtra(MainActivity.selectedCountry);

        TextView textView = findViewById(R.id.countryName);
        textView.setText(countryName);

        final Country country = CountryList.getCountry(countryName);
        ImageView flag = findViewById(R.id.flag);

        int imageResource = getResources().getIdentifier("@drawable/"+country.getmImgFile(), null, getPackageName());

        Drawable res = getResources().getDrawable(imageResource);
        flag.setImageDrawable(res);

        final TextView capitale = findViewById(R.id.capitale);
        final TextView langue = findViewById(R.id.langue);
        final TextView monnaie = findViewById(R.id.monnaie);
        final TextView population = findViewById(R.id.population);
        final TextView superficie = findViewById(R.id.superficie);

        capitale.setText(country.getmCapital());
        langue.setText(country.getmLanguage());
        monnaie.setText(country.getmCurrency());
        population.setText(Integer.toString(country.getmPopulation()));
        superficie.setText(Integer.toString(country.getmArea()));

        Button saveBtn = findViewById(R.id.savebtn);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CountryList.save(countryName, new Country(capitale.getText().toString(), country.getmImgFile().toString(), langue.getText().toString(), monnaie.getText().toString(), Integer.parseInt(population.getText().toString()), Integer.parseInt(superficie.getText().toString())));
                finish();
            }
        });
    }
}
